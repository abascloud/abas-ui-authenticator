/**
 * Sample tests
 */
import auth0 from 'auth0-js';
import Auth from '../../src/auth';
import KeyCloak from '../../src/lib/keycloak';

describe('auth', () => {
  it('typeof(Auth) should be a function', () => {
    expect(typeof (Auth)).toBe('function');
  });

  it.each([
    [
      {
        tenant: 'crystal-labs',
        client: 'client-hash',
        redirectUri: '/home',
        audience: '',
      },
      {
        origin: 'https://crystal-labs.localhost.eu.abas.ninja:8081',
        hostname: 'crystal-labs.localhost.eu.abas.ninja',
      },
      'https://crystal-labs.eu.abas.ninja/',
    ],
    [
      {
        tenant: 'crystal-labs',
        client: 'client-hash',
        redirectUri: '/home',
        audience: '',
      },
      {
        origin: 'https://crystal-labs.eu.abas.ninja',
      },
      'https://crystal-labs.eu.abas.ninja/',
    ],
  ])('getAudience', (args, windowLocation, expected) => {
    delete global.window.location;
    global.window = Object.create(window);
    global.window.location = windowLocation;
    const auth = new Auth(args);
    expect(auth.audience).toBe(expected);
  });

  it.each([
    [
      {
        tenant: 'crystal-labs',
        client: 'client-hash',
        redirectUri: '/home',
        audience: '',
      },
      auth0.WebAuth,
    ],
    [
      {
        tenant: '',
        hostname: 'crystal-labs.auth0.com',
        client: 'client-hash',
        redirectUri: '/home',
        audience: '',
      },
      auth0.WebAuth,
    ],
    [
      {
        tenant: '',
        hostname: '',
        client: 'client-hash',
        redirectUri: '/home',
        audience: '',
      },
      KeyCloak,
    ],
    [
      {
        tenant: '',
        hostname: '',
        authBaseUrl: 'auth0.abas.com',
        client: 'client-hash',
        redirectUri: '/home',
        audience: '',
      },
      KeyCloak,
    ],
  ])('getAuthenticator', (args, expected) => {
    try {
      const auth = new Auth(args);
      expect(auth.authenticator).toBeInstanceOf(expected);
    } catch (e) {
      expect(e.message).toBe("'authBaseUrl' is a required option");
    }
  });
});
