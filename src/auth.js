import auth0 from 'auth0-js';
import KeyCloak from './lib/keycloak';
import decodeBase64Url from './utils/decodeBase64Url';

let timer;
const assert = (condition, message) => {
  if (!condition) {
    throw new Error(message || 'Assertion failed');
  }
};

const getAudience = audience => audience
 || (window.location.origin.indexOf('localhost') >= 0
   ? `https://${window.location.hostname.split('.localhost').join('')}/`
   : `${window.location.origin}/`);

const getAuthenticator = ({ tenant, hostname, authBaseUrl }, authParam) => {
  // Select auth system depending on if onPrem or cloud/hybrid
  if (tenant || hostname) {
    return new auth0.WebAuth(
      Object.assign({}, authParam, { domain: hostname || `${tenant}.auth0.com` }),
    );
  }
  assert(authBaseUrl, "'authBaseUrl' is a required option");
  return new KeyCloak(
    Object.assign({}, authParam, { authBaseUrl }),
  );
};
export default class Auth {
  /**
   * Get ID Token
   */
  get token() {
    return this.idToken;
  }

  /**
   * Get Token if ready otherwise return null
   */
  get accessToken() {
    try {
      return this.authRes.accessToken;
    } catch (error) {
      return null;
    }
  }

  /**
   * Return decoded access token
   */
  get decodedAccessToken() {
    return this.accessToken ? JSON.parse(decodeBase64Url(this.accessToken.split('.')[1])) : null;
  }

  /**
   * Return userInfo
   */
  get user() {
    return this.userInfo;
  }

  /**
   * Return token expiration from 00:00:00 UTC on 1 January 1970 in seconds
   */
  get expiresAt() {
    return new Date(this.decodedAccessToken.exp * 1000);
  }

  /**
   * Try first silent Auth if this won't work go for force Login
   */
  async login() {
    const p = this.silentAuth();
    try {
      await p;
      return true;
    } catch (e) {
      this.forceLogin();
    }
    return false;
  }

  /**
   * This wil force the user to  login
   */
  forceLogin() {
    this.authenticator.authorize({
      connection: this.connection,
    });
  }

  /**
   * This will log the user immediately out
   */
  logout() {
    /* unset local auth data */
    this.authRes = null;
    this.userInfo = null;
    this.idToken = null;
    /* make auth provider logout */
    this.authenticator.logout({
      connection: this.connection,
      returnTo: this.redirectUri,
      federated: true,
    });
  }

  /**
   * Trigger to re-new the token
   */
  get isAuthenticated() {
    return this.accessToken && !this.isExpired();
  }

  /**
   * Check if user is in Admin group
   */
  get isAdmin() {
    return this.decodedAccessToken && (this.decodedAccessToken['http://abas.cloud/groups'] || []).includes('admin');
  }

  /**
   * only use in isAuthenticated
   */
  isExpired() {
    return new Date().getTime() >= (this.expiresAt.getTime() - this.expirationOffset);
  }

  /**
   * Store Auth result from Auth Provider
   * @param {*} authResult
   */
  storeAuthInfo(authResult) {
    this.authRes = authResult;
    this.idToken = authResult.idToken;
    this.userInfo = authResult.idTokenPayload;
  }

  /**
   * handle auth when returning from auth redirect
   */
  handleAuthentication() {
    return new Promise((resolve, reject) => {
      /* get token from url */
      this.authenticator.parseHash((err, authResult) => {
        const error = err || (authResult && authResult.error);
        if (error) {
          /* there was an error */
          if (window.location.origin === window.parent.location.origin) {
            /* tell all other windows that */
            window.parent.postMessage(error, window.location);
          }
          reject(error);
        } else if (authResult) {
          this.storeAuthInfo(authResult);
          /* auth was successfull/* */
          if (window.location.origin === window.parent.location.origin) {
            /* tell all other windows that */
            window.parent.postMessage(authResult, window.location);
          }
          resolve(authResult);
        } else {
          resolve(false);
        }
      });
    });
  }

  /**
   * Silent renew of token
   */
  silentAuth() {
    /**
     * Singleton start this process only once
     */
    if (this.silentAuthInProgress) {
      return this.silentAuthInProgress;
    }
    this.silentAuthInProgress = new Promise((resolve, reject) => {
      /**
       * Not sure if smart to overwrite Promise here
       */
      this.silentAuthInProgress = true;
      /*
       * Start renew of token
       */
      this.authenticator.renewAuth(
        {
          state: 'RENEW',
          usePostMessage: true,
          connection: this.connection,
          timeout: this.timeout,
        },
        (err, authResult) => {
          const error = err || (authResult && authResult.error);
          if (error) {
            /* ups smth went wrong */
            reject(error);
            this.silentAuthInProgress = null;
          } else if (authResult && authResult.idToken && authResult.idTokenPayload) {
            // make sure we don't store something that might not be the token
            this.storeAuthInfo(authResult);
            resolve();
            this.silentAuthInProgress = null;
          }
          // do not reset this.silentAuthInProgress if it's neither of these cases
        },
      );
    });
    return this.silentAuthInProgress;
  }

  /**
   * The constructor does not only create the Auth Object
   * it also starts the auth process
   *
   * token via url thing should be placed here
   * @param {*} options
   */
  constructor({
    audience,
    client,
    redirectUri,
    scope = 'openid profile email',
    connection,
    errorCb,
    timeout = 1000 * 5,
    tenant,
    hostname,
    authBaseUrl,
    expirationOffset = 1000 * 60 * 10,
    responseType = 'token id_token',
    refreshInterval = 1000 * 60,
  }) {
    assert(client, "'client' is a required option");
    /* hash value to identify the auth0/keyCloak tenant to log in */
    this.client = client;
    /* declare default value */
    this.authRes = null;
    this.idToken = null;
    this.userInfo = null;

    // redirectURL - redirect to after successfull login
    this.redirectUri = new window.URL(redirectUri, window.location.origin).href;

    // for example: dashboard-dev.eu.abas.ninja
    // to development on localhosts, localhost is cut out
    // because only real tenants are known by auth0
    this.audience = getAudience(audience);
    this.scope = scope;
    this.connection = connection;
    this.errorCb = errorCb;

    // maxiumum granted answer time for the login page from auth0 or keyCloak
    // security feature to avoid man in the middle attacks
    this.timeout = timeout;

    // the function isExpired is true ten minutes before the token is
    // expired. this should avoid that users are logged out during work
    this.expirationOffset = expirationOffset;

    const authParam = {
      clientID: this.client,
      redirectUri: this.redirectUri,
      audience: this.audience,
      responseType,
      scope: this.scope,
    };

    this.authenticator = getAuthenticator({ tenant, hostname, authBaseUrl }, authParam);

    // This could break refreshing if refreshInterval
    // is  greater than this.expirationOffset

    // Start a timed refresh operation if not done already
    // Check every this.refreshInterval if we are still authenticated
    //
    // Only execute if
    // 1. Not in iFrame
    // 2. In iFrame but parent window has different location than our iFrame
    // I believe iFrame checks were built in for the dashboard.
    // When dashboard was part of the webclient and displayed within an iFrame
    if (
      !timer
      && (window.parent === window || window.parent.location.origin !== window.location.origin)
    ) {
      const handleError = (cb, err) => (cb ? cb(err) : console.error(err));
      const refresh = async () => {
        if (!this.isAuthenticated) {
          const p = this.silentAuth();
          try {
            await p;
          } catch (err) {
            handleError(this.errorCb, err);
          }
        }
        timer = setTimeout(refresh, refreshInterval);
      };
      timer = setTimeout(refresh, refreshInterval);
    }
  }
}
