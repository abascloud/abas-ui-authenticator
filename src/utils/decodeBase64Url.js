const getCharCode = c => `00${c.charCodeAt(0).toString(16)}`.slice(-2);
const decodeBase64Url = (data) => {
  // convert base64url token (received from JWT) to base64
  const base64Token = data.replace(/_/g, '/').replace(/-/g, '+');
  const decodedString = atob(base64Token)
    // To support utf-8 character https://stackoverflow.com/questions/30106476/using-javascripts-atob-to-decode-base64-doesnt-properly-decode-utf-8-strings
    .split('')
    .map(c => `%${getCharCode(c)}`)
    .join('');
  return decodeURIComponent(decodedString);
};
export default decodeBase64Url;
