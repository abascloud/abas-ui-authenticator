interface AuthResult {
  idToken: string;
  idTokenPayload: object;
}
interface Options {
  tenant: string;
  client: string;
  redirectUri: string;
  connection: string;
  authBaseURL: string;
}
export declare class Auth {
  constructor(options: Options)
  token: string;
  accessToken: string;
  decodedAccessToken: string;
  user: string;
  expiresAt: Date;
  isAuthenticated: boolean;
  isAdmin: boolean;
  isExpired: boolean;
  login(): Promise<boolean>;
  forceLogin(): void;
  logout(): void;
  storeAuthInfo(authResult: AuthResult): void;
  handleAuthentication(): Promise<boolean>;
  silentAuth(): Promise<boolean>;
}

export default Auth;

// This is necessary for for the webpack bundle
// otherwise i need to create the Auth object with library.default()
export {
  Auth as Authentication,
};
