import decodeBase64Url from '../utils/decodeBase64Url';

export default class KeyCloak {
  constructor(options) {
    this.options = options;
  }

  logout(options) {
    const opt = Object.assign(this.options, options);
    const params = {
      redirect_uri: opt.redirectUri,
    };
    let qParams = '';
    Object.entries(params).forEach((arg) => {
      qParams += `${arg[0]}=${encodeURIComponent(arg[1])}`;
    });
    window.location.href = `${opt.authBaseUrl}/protocol/openid-connect/logout?${qParams}`;
  }

  authorize(options) {
    window.location.href = this.getAuthorizeUrl(options);
  }

  getAuthorizeUrl(options) {
    const opt = Object.assign(this.options, options);
    const params = {
      client_id: opt.clientID,
      redirect_uri: opt.redirectUri,
      response_type: opt.responseType,
      scope: opt.scope,
      nonce: Math.random().toString(36).substring(2, 5),
    };
    const qParams = Object.entries(params).map(entry => `${entry[0]}=${encodeURIComponent(entry[1])}`).join('&');
    return `${opt.authBaseUrl}/protocol/openid-connect/auth?${qParams}`;
  }

  renewAuth(options, cb) {
    const frame = document.createElement('iframe');
    frame.style.display = 'none';
    frame.src = this.getAuthorizeUrl();
    document.body.appendChild(frame);
    let lock = true;
    const cleanup = (err, data) => {
      cb(err, data);
      document.body.removeChild(frame);
      lock = false;
    };
    window.addEventListener('message', (e) => {
      if (lock) {
        if (e.data.accessToken || e.data.error) {
          cleanup(null, e.data);
        }
      }
    }, false);
    setTimeout(() => {
      if (lock) {
        cleanup(new Error(`timeout of ${options.timeout} ms exceeded`));
      }
    }, options.timeout);
  }

  // Disable eslint because we need to call parseHash on instance base (see handleAuthentication)
  // eslint-disable-next-line class-methods-use-this
  parseHash(cb) {
    try {
      if (window.location.hash === '') {
        cb(null, null);
        return;
      }
      // Parse the hash into JSON object
      const result = window.location.hash.substring(1).split('&') // Get array of parameter
        .map(queryParam => ({ [queryParam.split('=')[0]]: decodeURIComponent(queryParam.split('=')[1]) })) // Tranform query parameter in json objects
        .reduce((acc, entry) => Object.assign(acc, entry), {}); // Merge json objects

      if (result && result.access_token && result.id_token) {
        cb(null, { accessToken: result.access_token, idToken: result.id_token, idTokenPayload: JSON.parse(decodeBase64Url(result.id_token.split('.')[1])) });
      } else {
        // in case of containing hash part but without token
        cb(null, null);
      }
    } catch (err) {
      cb(err);
    }
  }
}
