import Auth from './auth';

export default Auth;

// This is necessary for for the webpack bundle
// otherwise i need to create the Auth object with library.default()
export {
  Auth as Authentication,
};
