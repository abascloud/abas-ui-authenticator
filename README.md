# abas-ui-authenticator

This ui authenticator handles onPremise and cloud authentication.

## How to use

You have to set the npm registry to our artifactory to be able to use the new ui-authenticator.

If you plan to integrate the authenticator in vue you can check this template project: https://bitbucket.org/abascloud/vue-template-project

### npm install

```npm install @abas/ui-authenticator```

### Constructor options

- `tenant`: Auth0 tenant (ex: abas-dev) => Required if using auth0
- `authBaseUrl`: Authentication server baseUrl to execute login flow => Required if using keycloak
- `redirectUri`: Uri to redirect to after login => Optionnal, (default: will use the url at login time)
- `audience`: OIDC audience (ex: https://crystal-labs.abas.ninja/) => Optionnal, (default: will compute it based on the window origin)
- `scope`: OIDC Scope (ex: 'openid email') => Optionnal, (default: 'openid profile email')
- `connection`: Auth0 database connection to use to authenticate user (ex: crystal-labs)
- `errorCb`: Callback to be called when an error happen => Optionnal
- `timeout`: Timeout for silent auth to be performed, in ms => Optionnal (default: 5000)
- `expirationOffset`: Offset to be used when checking token expiration, in ms => Optionnal (default: 600000)
- `responseType`: OIDC response => Optionnal (default: 'token id_token')
- `client`: OIDC client (ex:'qnBN5MjDwRZehAegb9JXRMVht59BssKO') => Required
- `refreshInterval`: Interval for token expiration check to be performed, in ms => Optionnal (default: 60000)


### handle-auth.js

```javascript

import Auth from '@abas/ui-authenticator';

// see below for Config example
import Config from './configuration';

this._initialize().then(() => {
    this._handleAuthentication();
});

_initialize() {
    return new Promise((resolve) => {
        Config.getConfig().then((configuration) => {
            const tenant = configuration.meta.auth0tenant;
            const redirectUri =  window.location.pathname + window.location.search;

            // atm, authBaseUrl is used only for onPremise
            const authBaseUrl = configuration.api.authBaseURL;
            const connection = configuration.meta.tenant;

            const client = configuration.clients.[YOUR_CLIENT];

            // set auth object (global, save into a store, ...)
            const auth = new Auth({tenant, client, redirectUri, connection, authBaseUrl });

            resolve();
          });
      });
  }

async _handleAuthentication(){
    let authenticated = await auth.handleAuthentication();

    if (!authenticated) {
      // use auth.forceLogin() if you want to ignore defined timeout
      authenticated = await auth.login();
    }

    // do your stuff to complete authentication
}

```

### config.js

```javascript

const configuration = {
    getConfig: () => window.fetch('/configuration.json')
    .then(response => response.json())
    .then((response) => {
        const [tenant] = window.location.hostname.split('.');
        response.meta.tenant = tenant;
        return response;
    }),
};

export default configuration;

```

## TODOS

- Add testing